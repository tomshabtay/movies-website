export class Movie {
    id: string;
    title: string;
    director: string;
    genre: string;
    runtime: string;
    year: string;
    posterUrl: string;


    generateId() {
        // generating hash id from title year runtime
        if (!this.year || !this.runtime || !this.title) {
            return
        }
        let numberOfTitleChar = 7
        var input = this.year + this.runtime + ""
        if (this.title.length > numberOfTitleChar) {
            input = input + this.title.substring(0, numberOfTitleChar)
        } else {
            input = input + this.title
        }

        var hash = 0, i, chr;
        if (input.length === 0) return hash;
        for (i = 0; i < input.length; i++) {
            chr = input.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }

        this.id = String(Math.abs(hash))
    }

    fillMovieData(m: any): boolean {
        this.title = m.Title
        this.year = m.Year
        this.runtime = m.Runtime.replace(' min', '')
        this.posterUrl = m.Poster
        this.generateId()
        this.director = m.Director
        this.genre = m.Genre
        
        if(!this.title) {
            return false;
        }
        return true
    }

}