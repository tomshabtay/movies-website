import { Component, OnInit, Input, Output, EventEmitter, Pipe ,PipeTransform } from '@angular/core';
import { Movie } from '../movie.model';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog'
import { MovieModalComponent } from '../movie-modal/movie-modal.component'
import { AlertModalComponent } from '../alert-modal/alert-modal.component'

@Component({
  selector: 'app-movie-row',
  templateUrl: './movie-row.component.html',
  styleUrls: ['./movie-row.component.css']
})
export class MovieRowComponent implements OnInit {
  @Input() movie: Movie
  @Output() deleted = new EventEmitter<Movie>();

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  openEdit() {
    const dialogConfig = new MatDialogConfig();
    // dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "70%";
    dialogConfig.height = "90%";
    dialogConfig.data = this.movie
    this.dialog.open(MovieModalComponent, dialogConfig)
  }

  deleteMovie() {
    const dialogConfig = new MatDialogConfig();
    // dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "30%";
    dialogConfig.data = {
      "msg": "Are you sure you want to delete: '" + this.movie.title + "' ?"
    }
    let dialogRef = this.dialog.open(AlertModalComponent, dialogConfig)
    dialogRef.afterClosed().subscribe((flag) => {
      console.log("Delete: " + flag)
      if (flag) {
        this.deleted.emit(this.movie)
      }
    })

  }
}
