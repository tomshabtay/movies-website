import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { MovieRowComponent } from '../movie-row/movie-row.component';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-alert-modal',
  templateUrl: './alert-modal.component.html',
  styleUrls: ['./alert-modal.component.css']
})
export class AlertModalComponent implements OnInit {

  msg: string
  constructor(private dialogRef: MatDialogRef<MovieRowComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }


  ngOnInit() {
    this.msg = this.data.msg
    console.log(this.msg)

  }

  ok() {
    this.dialogRef.close(true);
  }

  close() {
    this.dialogRef.close(false);
  }
}
