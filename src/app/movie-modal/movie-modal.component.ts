import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MovieRowComponent } from '../movie-row/movie-row.component';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Movie } from '../movie.model';

@Component({
  selector: 'app-movie-modal',
  templateUrl: './movie-modal.component.html',
  styleUrls: ['./movie-modal.component.css']
})
export class MovieModalComponent implements OnInit {
  movieCopy: Movie;
  movieRef: Movie;
  constructor(private dialogRef: MatDialogRef<MovieRowComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    console.log(this.data)
    this.movieCopy = JSON.parse(JSON.stringify(this.data)) as Movie
    this.movieRef = this.data as Movie
  }

  close() {
    console.log(this.movieCopy)
    this.dialogRef.close(false);
  }

  save() {
    try {
      this.movieRef.title = this.movieCopy.title
      this.movieRef.runtime = this.movieCopy.runtime
      this.movieRef.year = this.movieCopy.year
      this.movieRef.director = this.movieCopy.director
      this.movieRef.genre = this.movieCopy.genre
    } catch {

    }
    this.dialogRef.close(true);
  }

}
