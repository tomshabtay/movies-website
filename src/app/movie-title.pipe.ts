import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'movieTitle'
})
export class MovieTitlePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let input = value as string
    var newStr = input.toLowerCase()
    .split(' ')
    .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
    .join(' ');

    newStr = newStr.replace(/[^a-zA-Z ]/g, "")

    return newStr;
  }

}
