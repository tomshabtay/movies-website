import { Injectable } from '@angular/core';
import { Observable, of, forkJoin, from, Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Movie } from './movie.model'
import { v4 as uuid } from 'uuid';


@Injectable({
  providedIn: 'root'
})
export class MovieService {

  private apiKeyParam = "&apikey=c3a7ba66"
  private apiGetByTitleParam = "?t="
  private apiUrl = "http://www.omdbapi.com/"


  constructor(private http: HttpClient) {

  }

  getMovies(): Observable<string[]> {
    return of(this.moviesMock)
  }


  getMoviesFromApi() {

    var obsArr = new Array<any>()
    this.moviesMock.forEach((title)=> {
      let query = this.apiUrl + this.apiGetByTitleParam + title + this.apiKeyParam
      let obser = this.http.get(query)
      obsArr.push(obser)
    })
    
    return forkJoin(obsArr)
  }


  moviesMock = ["Back to the Future",
    "Desperado",
    "Kramer vs. Kramer",
    "The Manchurian Candidate",
    "Bull",
    "Heat",
    "About Schmidt",
    "Re-Animator",
    "Evolution",
    "Gone in 60 Seconds",
    "Wanted",
    "The Man with One Red Shoe",
    "The Jerk",
    "Whip It",
    "Spanking the Monkey"]
}
