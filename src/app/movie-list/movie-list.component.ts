import { Component, OnInit, Pipe } from '@angular/core';
import { MovieService } from '../movie.service';
import { Movie } from '../movie.model'
import { MatDialog, MatDialogConfig } from '@angular/material/dialog'
import { MovieModalComponent } from '../movie-modal/movie-modal.component'
import { AlertModalComponent } from '../alert-modal/alert-modal.component'

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {

  movies: Array<String>;
  movieArr = new Array<Movie>();

  constructor(private movieService: MovieService,
    private dialog: MatDialog) {
    this.getMovies();
    console.log(this.movieArr)
  }

  ngOnInit() {

  }

  deleteMovie(movie: Movie) {
    console.log("deleteMovie")
    console.log(movie)
    var index = this.movieArr.indexOf(movie);
    if (index > -1) {
      this.movieArr.splice(index, 1);
    }
  }

  addMovie() {
    var movie = new Movie()

    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = "75%";
    dialogConfig.data = movie
    let ref = this.dialog.open(MovieModalComponent, dialogConfig)
    ref.afterClosed().subscribe((isSave) => {
      if (isSave) {
        var found = false
        this.movieArr.forEach(element => {
          console.log(element.title)
          console.log(movie.title)
          if(element.title.toLowerCase().trim() == movie.title.toLowerCase().trim()){
            found = true;
            const dialogConfig = new MatDialogConfig();
            dialogConfig.autoFocus = true;
            dialogConfig.width = "30%";
            dialogConfig.data = {
              "msg": "Movie already exists."
            }
            let ref = this.dialog.open(AlertModalComponent, dialogConfig)
            
          }
        });

        if(!found){this.movieArr.push(movie)}
        
      }
    })
  }


  getMovies() {
    this.movieService.getMoviesFromApi().subscribe((items) => {
      items.forEach(item => {
        let m = item as any
        let movie = new Movie();
        if (movie.fillMovieData(m)) {
          this.movieArr.push(movie)
        }
      })
    })
  }




}
