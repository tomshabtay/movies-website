import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; 

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { MovieListComponent } from './movie-list/movie-list.component';
import { MovieRowComponent } from './movie-row/movie-row.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MovieModalComponent } from './movie-modal/movie-modal.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { AlertModalComponent } from './alert-modal/alert-modal.component';
import { MovieTitlePipe } from './movie-title.pipe';
@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    MovieListComponent,
    MovieRowComponent,
    MovieModalComponent,
    AlertModalComponent,
    MovieTitlePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatDialogModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  providers: [],
  entryComponents:[MovieModalComponent,AlertModalComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
